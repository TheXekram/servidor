//rutas para autenticar
const express = require('express');
const router = express.Router();
const { check } = require('express-validator');
const auth = require('../middleware/auth');
const authController = require('../controllers/authController');

//iniciar un usuario
//api/auth
router.post('/',
    authController.autenticarUsuario
);

//obtener el usuario autenticado
router.get('/',
    auth,
    authController.usuarioAutenticado
)
module.exports = router;