const express = require('express');
const router = express.Router();
const tareaController = require('../controllers/tareaController');
const auth = require('../middleware/auth');
const { check } = require('express-validator')

// crea proyectos 
// api/tarea
router.post('/',
    auth,
    [
        check('nombre', 'El nombre es obligatorio').not().isEmpty(),
        check('proyecto', 'El proyecto es obligatorio').not().isEmpty()
    ],
    tareaController.CrearTarea
)
// Obtener las tareas del proyecto 
router.get('/',
    auth,
    tareaController.obtenerTareas
)
//actulizar las tareas del proyecto
router.put('/:id',
    auth,
    tareaController.actualizarTareas
)

//Eliminar Tarea
router.delete('/:id',
    auth,
    tareaController.eliminarTarea
)
module.exports = router;