const Tarea = require('../models/Tarea');
const Proyecto = require('../models/Proyecto');
const { validationResult } = require('express-validator');


//crea una nueva tarea
exports.CrearTarea = async (req, res) => {
    //Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({errores: errores.array()})
    }

    try {
        // extraemos el proyecto y comprobar si existe 
    
        const { proyecto } = req.body;
        
        const existeproyecto = await Proyecto.findById(proyecto);
        if(!existeproyecto) {
            return res.status(404).json({ msg:"Proyecto no encontrado" })
        }

        //verficar el creador del proyecto para ver si le perytenece al creador
        if(existeproyecto.creador.toString() !== req.usuario.id ) {
            return res.status(401).json({ msg: "Usuario no Autorizado" });
        }
        //creamos la tarea
        const tarea = new Tarea(req.body);
        await tarea.save();
        res.json({ tarea });

    } catch (error) {
        console.log(error);
        res.status(500).send("hubo un error");        
    }
}


//Obtener las tareas del proyecto
exports.obtenerTareas = async (req, res) => {
    try {
        // extraemos el proyecto y comprobar si existe 
    
        const { proyecto } = req.body;
        
        const existeproyecto = await Proyecto.findById(proyecto);
        if(!existeproyecto) {
            return res.status(404).json({ msg:"Proyecto no encontrado" })
        }

        //verficar el creador del proyecto para ver si le perytenece al creador
        if(existeproyecto.creador.toString() !== req.usuario.id ) {
            return res.status(401).json({ msg: "Usuario no Autorizado" });
        }

        const tareas = await Tarea.find({ proyecto });
        res.json({ tareas })
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error s servidor');
        
    }
}

//actualizar tareas
exports.actualizarTareas = async (req, res) => {
    try {
        const { proyecto, nombre, estado } = req.body;        
        //si existe la tarea
        let tareaExiste = await Tarea.findById(req.params.id);
        
        if(!tareaExiste) {
            return res.status(404).json({ msg: 'No existe esa tarea' })
        }
        //extraer proyecto
        const existeproyecto = await Proyecto.findById(proyecto);
        //verficar el creador del proyecto para ver si le perytenece al creador
        if(existeproyecto.creador.toString() !== req.usuario.id ) {
            return res.status(401).json({ msg: "Usuario no Autorizado" });
        }
        //crear un objeto con la nueva informacion
        const nuevaTarea = {};
        if(nombre) {
            nuevaTarea.nombre = nombre;
        }if(estado) { 
            nuevaTarea.estado = estado;
        }
        //guardar la tarea
        const tarea = await Tarea.findByIdAndUpdate({_id: req.params.id}, nuevaTarea, {new: true});

        res.json({tarea})
    } catch (error) {
        console.log(error);
        res.status(500).send('error en el server')
    }
}

//eliminar Tareas 
exports.eliminarTarea = async (req, res) => {
    try {
        const { proyecto } = req.body;        
        //si existe la tarea
        let tareaExiste = await Tarea.findById(req.params.id);
        
        if(!tareaExiste) {
            return res.status(404).json({ msg: 'No existe esa tarea' })
        }
        //extraer proyecto
        const existeproyecto = await Proyecto.findById(proyecto);
        //verficar el creador del proyecto para ver si le perytenece al creador
        if(existeproyecto.creador.toString() !== req.usuario.id ) {
            return res.status(401).json({ msg: "Usuario no Autorizado" });
        }
        
        //eliminar
        await Tarea.findOneAndRemove({_id: req.params.id});

        res.json({  msg: 'Tarea Eliminada'});
    } catch (error) {
        console.log(error);
        res.status(500).send('error al eliminar')
        
    }
}