const Usuario = require('../models/Usuario');
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator');
const jwt = require('jsonwebtoken');

exports.crearUsuario = async (req, res) => {

    //revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty() ){
        return res.status(400).json({errores: errores.array() });
    }


    //realizar destrugturing del usuario
    const {email, password} = req.body;
    
    try {
        let usuario =await Usuario.findOne({ email });

        //realizamos la busqyeda del usaurio
        if(usuario) {
            return res.status(400).json({ msg: 'El usuario ya existe' });
        }


        //crea un nuevo usaurio
        usuario = new Usuario(req.body);

        //hashear el pasword
        const salt = await bcryptjs.genSalt(10);
        usuario.password = await bcryptjs.hash(password, salt);

        //guardar usuario
        await usuario.save();

        //Crear y firmar el JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };
        jwt.sign(payload, process.env.SECRETA, {
            expiresIn: 3600
        }, (error, token) => {
            if(error) throw error;

            //Mensaje de confirmacion
            res.json({ token })
        });

    } catch (error) {
        console.log(error);
        res.status(400).send('Hubo un error')
    }
}