const Proyecto = require('../models/Proyecto');
const { validationResult } = require('express-validator');

exports.crearProyecto = async (req, res) => {

    //Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({errores: errores.array()})
    }

    try {
        //crear un nuevo proyecto
        const proyecto = new Proyecto(req.body);

        //Guardar el creador via JWT
        proyecto.creador = req.usuario.id

        //guardamis el proyecto
        proyecto.save();
        res.json(proyecto);
        
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error')
    }
}

//obtener todos los proyectos del usuario
exports.obtenerProyecto = async (req, res) => {
    try {
        const proyectos = await Proyecto.find({ creador: req.usuario.id }).sort({ creado: -1 })
        res.json({ proyectos })
    } catch (error) {
        console.log(error);
        res.status(500).send("hubo un error");
        
    }
}

//actuliazar el proyecto
exports.actualizarProyecto = async (req,res) => {
    //Revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty()){
        return res.status(400).json({errores: errores.array()})
    }
    // extraer la informacion del proyecto
    const { nombre } = req.body;
    //colocar el nuevo nombre del proyecto
    const nuevoProyecto = {};

    if(nombre) {
        nuevoProyecto.nombre = nombre;
    }

    //realizar el update

    try {
        //revisar el ID
            let proyecto = await Proyecto.findById(req.params.id);
        // si el proyecto existe o no
            if(!proyecto) {
                return res.status(404).json({ msg: "Proyecto no encontrado" });
            }

        //verficar el creador del proyecto
            if(proyecto.creador.toString() !== req.usuario.id ) {
                return res.status(401).json({ msg: "Usuario no Autorizado" });
            }

        //actualizar
            proyecto = await Proyecto.findByIdAndUpdate({_id: req.params.id }, { $set : nuevoProyecto}, {new: true});

            res.json({proyecto});
        
    } catch (error) {
        console.log(error);
        res.status(500).send('Hubo un error')
        
    }
}

//eliminar el [rpyecto x su ID
exports.eliminarProyecto = async (req, res) => {
    try {
        //revisar el ID
        let proyecto = await Proyecto.findById(req.params.id);
        // si el proyecto existe o no
            if(!proyecto) {
                return res.status(404).json({ msg: "Proyecto no encontrado" });
            }

        //verficar el creador del proyecto
            if(proyecto.creador.toString() !== req.usuario.id ) {
                return res.status(401).json({ msg: "Usuario no Autorizado" });
            }
        //eliminar 
            proyecto = await Proyecto.findOneAndRemove({ _id: req.params.id });
            res.json({ msg: "proyecto eliminado" })
    } catch (error) {
        console.log(error);
        res.status(500).send("Hubo un error en el servidor")        
    }
}
